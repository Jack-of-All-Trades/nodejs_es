import * as chai from 'chai';
import chaiHttp = require('chai-http');
import 'mocha';
import "reflect-metadata";
const app = require('../../src').default;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
process.env.NODE_ENV = 'test';
chai.use(chaiHttp);
const expect = chai.expect;

describe('/people-like-you', () => {
    it('Test 1: Should return all matching results based on query params', async () => {
        const results = await chai.request(app).get('/people-like-you?age=23&latitude=40.71667&longitude=19.56667&monthlyIncome=5500&experienced=false');
        expect(results).to.have.status(200)
        expect(results.body)
        .to.have.property('peopleLikeYou')
        expect(results.body.peopleLikeYou)
        .to.be.an.instanceof(Array)
        .to.have.lengthOf.above(0);
    })
})

describe('/people-like-you', () => {
    it('Test 2: Should return all matching results based on query params', async () => {
        const results = await chai.request(app).get('/people-like-you?latitude=54.2877219&longitude=17.6311856&experienced=false');
        expect(results).to.have.status(200)
        expect(results.body)
        .to.have.property('peopleLikeYou')
        expect(results.body.peopleLikeYou)
        .to.be.an.instanceof(Array)
        .to.have.lengthOf.above(0);
    })
})

describe('/people-like-you', () => {
    it('Test 2: Should return empty results', async () => {
        const results = await chai.request(app).get('/people-like-you?age=1000');
        expect(results).to.have.status(200)
        expect(results.body)
        .to.have.property('peopleLikeYou')
        .to.be.empty
    })
})