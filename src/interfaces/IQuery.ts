export interface IQuery {
    name?: string
    age?: number
    latitude?: number
    longitude?: number
    monthlyIncome?: number
    experienced?: boolean
}