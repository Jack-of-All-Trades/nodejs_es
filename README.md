## Simple NodeJS app w/ Elasticsearch

# Development
Run: docker-compose build --parallel --no-cache --build-arg NPM_RUN=dev && docker-compose up -d

# Production
Run: docker-compose build --parallel --no-cache --build-arg NPM_RUN=prod && docker-compose up -d

# Explanation

This sample project illustrates the use of Api. I have used postman app to test the API. 

Application is running at http://134.209.102.19