#!/bin/sh

while getopts e:h: option 
do 
 case "${option}" 
 in 
 e) NPM_RUN=${OPTARG};; 
 h) HOST=${OPTARG};;
 esac 
done

until curl $HOST 2> /dev/null; do
  echo "Waiting for elasticsearch..."
  sleep 5;
done

sleep 100 && npm run $NPM_RUN