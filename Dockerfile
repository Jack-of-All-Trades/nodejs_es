FROM node:8.15.1-alpine

ARG NPM_RUN

ARG HOST

ENV HOST=${HOST}

ENV NPM_RUN "${NPM_RUN}"

COPY . /app

WORKDIR /app

RUN apk update && apk add curl ca-certificates && rm -rf /var/cache/apk/* && npm i

RUN chmod 775 /app/script.sh

EXPOSE 3555

CMD sh ./script.sh -e ${NPM_RUN} -h ${HOST}